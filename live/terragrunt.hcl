terraform {
  extra_arguments "common_vars_plan" {
    commands = [
      "plan"]

  }

  extra_arguments "common_vars_autoapprouve" {
    commands = [
      "apply","destroy"]

    arguments = [
      "-auto-approve"
    ]
  }
}
remote_state {
  backend = "http"
  generate = {
    path      = "backend.generated.tf"
    if_exists = "overwrite"
  }
  config = {
    address = "https://gitlab.com/api/v4/projects/${project_id}/terraform/state/$${replace(replace(path_relative_to_include(), ".", "-"), "/", "_")}.tfstate"
    retry_wait_min = "5"
    username = "${project_user}"
    password = get_env("gitlab_token")
  }
}
